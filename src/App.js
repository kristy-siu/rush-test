import React, { Component } from 'react';

import './App.css';

import FixtureBlock from './elements/FixtureBlock'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Fixtures App</h1>
        </header>
        <p className="App-intro">
          Fixtures app to show live fixture changes
        </p>
          <div id="App-content">
            <FixtureBlock/>
          </div>
      </div>
    );
  }
}

export default App;
