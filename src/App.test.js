import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/*
   Mock up to simulate axios call during tests
 */

jest.mock('axios', () => {
    let exampleData = require('./example_data.json');

    return {
        get: jest.fn(() => Promise.resolve(exampleData)),
    };
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
