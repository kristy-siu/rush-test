/**
 * Created by kwss on 08/06/2018.
 */
import React from 'react';
import ReactDOM from 'react-dom';

import Player from './Player';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

let example_player = {
    "name": "Eric Dier",
    "formation_place": 4,
    "type": "Midfielder",
    "position": "CM"
};

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Player player={example_player} />, div);
    ReactDOM.unmountComponentAtNode(div);

});

it('renders content correctly', () => {

    let expected_content1 = <p>Eric Dier </p>;
    let expected_content2 = <span>CM</span>;
    let expected_content3 = <p>Midfielder</p>;
    let unexpected_content = <p>Goalkeeper</p>;
    const wrapper = mount((
        <Player player={example_player}/>
    ));

    expect(wrapper.contains(expected_content1)).toBe(true);
    expect(wrapper.contains(expected_content2)).toBe(true);
    expect(wrapper.contains(expected_content3)).toBe(true);
    expect(wrapper.contains(unexpected_content)).toBe(false);
});