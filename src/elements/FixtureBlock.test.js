/**
 * Created by kwss on 04/06/2018.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import { shallow, configure } from 'enzyme';

import Adapter from 'enzyme-adapter-react-16';

import FixtureBlock from './FixtureBlock';
import {Rows} from './FixtureBlock';


import settings from '../settings.json';

configure({adapter: new Adapter()});

const exampleData = require('../example_data.json');

/*
 Mock up to simulate axios call during tests
 */

jest.mock('axios', () => {
    let exampleData = require('../example_data.json');

    return {
        get: jest.fn(() => Promise.resolve({
            data: exampleData.lineups.correct
        })),
    };
});

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<FixtureBlock />, div);
    ReactDOM.unmountComponentAtNode(div);

});


it('fetch fixtures on #componentDidMount', () => {

    const fixtureBlock = shallow(<FixtureBlock />);

    fixtureBlock
        .instance()
        .componentDidMount().then(() => {
        expect(axios.get).toHaveBeenCalled();
        expect(axios.get).toHaveBeenCalledWith(settings.endpoints.fixtures);

        expect(fixtureBlock.instance().state).toHaveProperty('data', exampleData.lineups.correct)
    });
});

it('errors when duplicate formation places are found', () => {
    const example = exampleData.lineups.duplicate;
    const props = {formation:example.formation,  players: example.players};
    expect( () => Rows(props)).toThrow(Error);
});

it('errors when missing formation places are found', () => {
    const example = exampleData.lineups.missing;
    const props = {formation:example.formation,  players: example.players};
    expect( () => Rows(props)).toThrow(Error);
});

it('creates formation rows without crashing', () => {
    const example = exampleData.lineups.correct;
    const props = {formation:example.formation,  players: example.players};
    Rows(props);
});
