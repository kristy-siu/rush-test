/**
 * Created by kwss on 04/06/2018.
 */

import React, { Component } from 'react';
import axios from 'axios';
import Pusher from 'pusher-js';



import bg from '../pitch-bg.png';
import settings from '../settings.json';
import orders from '../fixture_orders.json';

import ErrorBoundary from '../ErrorBoundary';
import Player from './Player';

import './FixtureBlock.css';

let divStyle = {
    backgroundImage: 'url('+bg+')',
};

/**
 * Container for formation table
 */
class FixtureBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {data: null};
    }

    componentDidMount() {
        this.mounted = true;
        // Initial Fetch
        return axios.get(settings.endpoints.fixtures).then(res => {
            if (this.mounted) {
                this.setState({data: res.data});
                const socket = new Pusher(settings.pusher.APP_KEY, {
                    cluster: settings.pusher.APP_CLUSTER,
                });
                const channel = socket.subscribe(settings.pusher.Channel);
                let that = this;
                channel.bind(settings.pusher.Event, function (data) {
                    that.updateState(data);
                });
            }

        });

    }
    updateState(new_state_data) {
        this.setState({data: new_state_data})
    }
    componentWillUnmount() {
        this.mounted = false;
    }
    render() {

        return (
            <div style={divStyle} className="Fixture-block">
                {
                    this.state.hasOwnProperty('data') && this.state.data !== null &&
                    <h2> { this.state.data.team }  - { this.state.data.formation }</h2>
                }
                <ErrorBoundary>
                {
                    this.state.hasOwnProperty('data') && this.state.data !== null &&
                    <div className="formation"> <Rows formation={this.state.data.formation} players={this.state.data.players}/> </div>
                }
                </ErrorBoundary>
            </div>
        );

    }
}


/*
 Render rows of players based on formation data
 */
function Rows(props) {
    // List of rows of players
    let rows = [];
    // Get the formation with the goalie included
    let fd = ("1"+props.formation).split('').map(n => parseInt(n, 10));
    // Get the order of the formation places for this formation
    let order = orders.orders[props.formation];
    // Get the players from the properties
    let players = props.players;

    // Calculate max width of a row in this formation
    let max_width = 0;
    for (let f in fd) {
        if (max_width < fd[f]) {
            max_width = fd[f];
        }
    }


    let player_number = 0;

    // For each line in the formation
    for (let f in fd) {
        let row_players = fd[f];
        let row = [];
        let width = row_players;
        // handle rows with a single player in them
        if (row_players === 1){
            width = max_width +1;

        }
        // Calculate margin
        let margin = 0;

            margin = ((max_width - row_players));

        for (let x=0; x < row_players; x++) {

            let matched_player = players.filter(
                function(player) {
                    return player.formation_place === this;
                }, order[player_number]);
            let player = null;


            if (matched_player.length > 1) {
                throw Error(`Duplicate player position: ${ order[player_number] }`);
            }
            else if (matched_player.length < 1) {
                throw Error(`Player for position ${ order[player_number] } not found`);
            }
            else {
                player = matched_player[0];
            }
            if (row_players === 1)
                row.push(<div key={`${ rows.length }${ row.length }`} className={"Row width"+width+" ml"+margin+" mr"+margin}><Player player={player} /></div>);
            else
                row.push(<div key={`${ rows.length }${ row.length }`} className={"Row width"+width}><Player player={player} /></div>);


            player_number++;
        }
        // Add the row to the formation
        rows.push(<div key={rows.length} > { row } </div>);
    }
    return rows;

}


export default FixtureBlock;
export { Rows };