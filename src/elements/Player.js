/**
 * Created by kwss on 08/06/2018.
 */

import React, { Component } from 'react';

import shirt from '../player-shirt.png';

import './Player.css';

let divStyle = {
    backgroundImage: 'url('+shirt+')',
};

class Player extends Component {

    render() {
        return <div style={divStyle} className="player">
            <p>{this.props.player.name} </p>
            <span>{this.props.player.position}</span>
            <p>{this.props.player.type}</p>
        </div>;
    }
}


export default Player;